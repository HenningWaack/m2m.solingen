data "aws_ami" "lobsters_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["n6consulting/artful/workshop-m2m-*"]
  }

  owners = ["001356815600"]
}

variable "lobsters_port" {
  description = "Port used on the Rails servers running Lobsters"
  default     = 3000
}

variable "port-lobsters" {
  default = "3000"
}

resource "aws_db_instance" "db-lobsters" {
  allocated_storage      = 10
  storage_type           = "gp2"
  engine                 = "mariadb"
  engine_version         = "10.0.24"
  instance_class         = "db.t2.micro"
  name                   = "dblobsters${var.student_id}"
  username               = "${var.database_username}"
  password               = "${var.database_password}"
  db_subnet_group_name   = "${aws_db_subnet_group.subg-db.name}"
  skip_final_snapshot    = true
  vpc_security_group_ids = ["${aws_security_group.sg-db.id}"]

  tags {
    Name = "db-lobsters-${var.student_id}"
  }
}

resource "aws_lb" "lb-lobsters" {
  name               = "lb-lobsters-${var.student_id}"
  security_groups    = ["${aws_security_group.sg-fe.id}"]
  internal           = false
  load_balancer_type = "application"

  subnets = ["${aws_subnet.subnet-app-1.id}",
    "${aws_subnet.subnet-app-2.id}",
  ]

  tags {
    Name = "lb-lobsters-${var.student_id}"
  }
}

resource "aws_lb_target_group" "tg-lobsters" {
  name        = "tg-lobsters-${var.student_id}"
  port        = "${var.port-lobsters}"
  protocol    = "HTTP"
  vpc_id      = "${aws_vpc.vpc.id}"
  target_type = "instance"
}

resource "aws_lb_listener" "listen-lobsters" {
  load_balancer_arn = "${aws_lb.lb-lobsters.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.tg-lobsters.arn}"
    type             = "forward"
  }
}

data "template_file" "lobsters-database-yml" {
  template = "${file("templates/database.yml")}"

  vars {
    database_host            = "${aws_db_instance.db-lobsters.address}"
    database_name            = "student${var.student_id}db"
    database_master_user     = "${var.database_username}"
    database_master_password = "${var.database_password}"
  }
}

# Modify this data block
data "template_file" "lobsters-production-rb" {
  template = "${file("templates/production.rb")}"

  vars {
    request_service_lb = "${aws_lb.lb-request.dns_name}"
  }
}


data "template_file" "influxdb-rails-initializer" {
  template = "${file("${path.module}/templates/influxdb-rails.rb")}"

  vars {
    influxdb_host     = "${var.influxdb_host}"
    influxdb_database = "${var.influxdb_database}"
  }
}


data "template_file" "cloud-init-lobsters" {
  template = "${file("${path.module}/templates/lobsters-cloud-init.tpl")}"

  vars {
    database_host            = "${aws_db_instance.db-lobsters.address}"
    database_name            = "dblobsters${var.student_id}"
    database_master_user     = "${var.database_username}"
    database_master_password = "${var.database_password}"


    telegraf_content          = "${base64encode("${data.template_file.telegraf-config.rendered}")}"
    database_yml_content  = "${base64encode("${data.template_file.lobsters-database-yml.rendered}")}"
    production_rb_content = "${base64encode("${data.template_file.lobsters-production-rb.rendered}")}"
    influxdb_content = "${base64encode("${data.template_file.influxdb.rendered}")}"
  }
}

resource "aws_launch_configuration" "lc-lobsters" {
  name_prefix                 = "lb-lobsters-${var.student_id}-"
  image_id                    = "${data.aws_ami.lobsters_ami.id}"
  instance_type               = "t2.micro"
  key_name                    = "${aws_key_pair.keypair.key_name}"
  security_groups             = ["${aws_security_group.sg-apps.id}"]
  associate_public_ip_address = true

  user_data = "${data.template_file.cloud-init-lobsters.rendered}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg-lobsters" {
  name                 = "asg-lobsters-${var.student_id}"
  launch_configuration = "${aws_launch_configuration.lc-lobsters.id}"

  vpc_zone_identifier = ["${aws_subnet.subnet-app-1.id}",
    "${aws_subnet.subnet-app-2.id}",
  ]

  min_size     = 2
  max_size     = 2
  force_delete = true

  target_group_arns = ["${aws_lb_target_group.tg-lobsters.arn}"]

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Owner"
    value               = "student-${var.student_id}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Name"
    value               = "asg-lobsters-${var.student_id}"
    propagate_at_launch = true
  }
}

output "lobsters-dns-name" {
  value = "${aws_lb.lb-lobsters.dns_name}"
}

output "lobsters-db" {
  value = "${aws_db_instance.db-lobsters.address}"
}
