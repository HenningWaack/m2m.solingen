#cloud-config
write_files:
-   encoding: b64
    path:  /home/ubuntu/run-router.sh
    content: "${router_run_script}"
    permissions: '0755'

packages:
  - awscli
  - nodejs
  - npm

runcmd:
- mkdir -p /home/ubuntu/router
- echo "downloading tgz from s3://${router_service_bucket}/router.tgz"
- AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${router_service_bucket}/router.tgz" /home/ubuntu/router/
- echo "downloaded tgz"
- cd /home/ubuntu/router && tar zxvf router.tgz
- cd /home/ubuntu/router/m2m.router && npm install
- cd /home/ubuntu/ && ./run-router.sh
