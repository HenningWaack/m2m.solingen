#cloud-config
write_files:
-   encoding: b64
    owner: ubuntu:ubuntu
    path:  /home/ubuntu/lobsters/config/database.yml
    content: "${database_yml_content}"
    permissions: '0644'
-   encoding: b64
    owner: ubuntu:ubuntu
    path:  /home/ubuntu/lobsters/config/environments/production.rb
    content: "${production_rb_content}"
    permissions: '0644'
-   encoding: b64
    owner: ubuntu:ubuntu
    path:  /etc/telegraf/telegraf.d/monitoring.conf
    content: "${influxdb_content}"
    permissions: '0644'

runcmd:
- wget https://dl.influxdata.com/telegraf/releases/telegraf_1.6.3-1_amd64.deb
- sudo dpkg -i telegraf_1.6.3-1_amd64.deb
- su -l -c "cd lobsters && rake db:setup RAILS_ENV=production" ubuntu
- su -l -c "cd lobsters && rails server -b 0.0.0.0 -e production" ubuntu