InfluxDB::Rails.configure do |config|
  config.influxdb_database = "${influxdb_database}"
  config.influxdb_username = "root"
  config.influxdb_password = "root"
  config.influxdb_hosts    = ["${influxdb_host}"]
  config.influxdb_port     = 8086
end
