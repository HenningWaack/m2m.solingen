variable "influxdb_host" {
  description = "IP address or host name of the InfluxDB back end"
  default     = "ec2-18-184-49-169.eu-central-1.compute.amazonaws.com"
}

variable "influxdb_database" {
  description = "Database name to use in the InfluxDB back end"
  default     = "solingen"
}

data "template_file" "telegraf-config" {
  template = "${file("templates/telegraf.conf")}"

  vars {
    influxdb_host     = "${var.influxdb_host}"
    influxdb_database = "${var.influxdb_database}"
  }
}
