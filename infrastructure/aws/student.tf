# These are variables that you must fill in before you can deploy infrastructure.

variable "student_id" {
  description = "A unique ID that separates your resources from everyone else's"
  default     = "qvn43hrsfcmgrke5pk"
}

variable "aws_region" {
  description = "The AWS Region to use"
  default     = "eu-central-1"
}

variable "database_username" {
  description = "Your chosen master user name for the database. Must be less than 16 characters and may only use a-z, A-Z, 0-9, '$' and '_'. The first character cannot be a digit."
  default     = "dbuser"
}

variable "database_password" {
  description = "Your chosen master user password for the database. Must be at least 8 characters, but not contain '/', '\\', \", or '@'."
  default     = "Sa7h9T7LAns3ZkD2"
}

variable "public_key" {
  description = "The public half of your SSH key. Make sure it is in OpenSSH format (it should start with 'ssh', not 'BEGIN PUBLIC KEY')"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDZSW8yADnDkkiWte6I4JysfN62kh2K1mbQzHFQzdZGNMy/KTF8m60yPDIlkM7Hhvswgen7S533MBoL13xsCZf+2s1N7S3i8PBH2kEyySsc5fPiiSli7Yfug1QVn43HRsfCMGrKE5pKcxvQUKZRXmtLtds1EFSwtALf6gwDCgV/JQ== henningwaack@Hennings-MacBook-Pro.local"
}
