var express = require('express')
var proxy = require('express-http-proxy')
const url = require('url')
var app = express()

app.use('/modlog', proxy(process.env.MODLOG_DNS_NAME))
app.use('/*', proxy(process.env.LOBSTER_DNS_NAME, {
    proxyReqPathResolver: function(req) {
        var path = url.parse(req.baseUrl).path;
        //console.log("Hit path: '" + path + "'.");
        if( path === "null") {
            return;
        }
        return path;
    },
    preserveHostHdr: true
}))

app.listen(1972, () => console.log("node.js router is listening on port 1972"))